package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void register_oneCustomer_getOneCustomer() {
        Customer customer = new Customer("1");
        Cuisine cuisine = new Cuisine("french");
        cuisinesRegistry.register(customer, cuisine);

        List<Customer> customers = cuisinesRegistry.cuisineCustomers(cuisine);
        assertEquals(1, customers.size());
        assertThat(customers, containsInAnyOrder(customer));
    }

    @Test
    public void register_twoCustomerToOneCuisines_getTwoCustomers() {
        Customer customer1 = new Customer("1");
        Customer customer2 = new Customer("2");
        Cuisine cuisine = new Cuisine("french");
        cuisinesRegistry.register(customer1, cuisine);
        cuisinesRegistry.register(customer2, cuisine);

        List<Customer> customers = cuisinesRegistry.cuisineCustomers(cuisine);
        assertEquals(2, customers.size());
        assertThat(customers, containsInAnyOrder(customer1, customer2));
    }

    @Test
    public void register_oneCustomerToTwoCuisines_getTwoCuisines() {
        Customer customer = new Customer("1");
        Cuisine cuisine1 = new Cuisine("french");
        Cuisine cuisine2 = new Cuisine("german");
        cuisinesRegistry.register(customer, cuisine1);
        cuisinesRegistry.register(customer, cuisine2);

        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(customer);
        assertEquals(2, cuisines.size());
        assertThat(cuisines, containsInAnyOrder(cuisine1, cuisine2));
    }

    @Test
    public void cuisineCustomers_sendNull_getNull() {
        assertNull(cuisinesRegistry.cuisineCustomers(null));
    }

    @Test
    public void customerCuisines_sendNull_getNull() {
        assertNull(cuisinesRegistry.customerCuisines(null));
    }

    @Test
    public void topCuisines_requestTop1_getTop1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("french"));
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(1);
        assertEquals(1, cuisines.size());
        assertThat(cuisines, containsInAnyOrder(new Cuisine("french")));

    }

    @Test
    public void topCuisines_requestTop2_getTop2() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("french"));
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(2);
        assertEquals(2, cuisines.size());
        assertThat(cuisines, containsInAnyOrder(new Cuisine("french"), new Cuisine("german")));

    }


}