package de.quandoo.recruitment.registry.model;

public class Cuisine {

    private final String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object object) {
        return name.equals(((Cuisine) object).name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
