package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final static List<String> cuisines = Arrays.asList("italian", "french", "german");
    private final Map<Cuisine, Set<Customer>> cuisineMap;

    InMemoryCuisinesRegistry() {
        cuisineMap = new HashMap<>();
        for (String cuisine : cuisines) {
            cuisineMap.put(new Cuisine(cuisine), new HashSet<>());
        }
    }

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        if (cuisineMap.containsKey(cuisine)) {
            cuisineMap.get(cuisine).add(userId);
        } else {
            throw new InvalidParameterException("Unknown cuisine, please contact our support");
        }
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisineMap.containsKey(cuisine)) {
            return new ArrayList<>(cuisineMap.get(cuisine));
        }

        return null;
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        List<Cuisine> customerCuisines = new ArrayList<>();

        for (Map.Entry<Cuisine, Set<Customer>> entry:cuisineMap.entrySet()) {
            if (entry.getValue().contains(customer)) {
                customerCuisines.add(entry.getKey());
            }
        }
        return customerCuisines.isEmpty()? null : customerCuisines;
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return getCuisinesSortedByPopularity().subList(0, n);
    }

    private List<Cuisine> getCuisinesSortedByPopularity() {
        return cuisineMap
                .entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().size() - o1.getValue().size())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
